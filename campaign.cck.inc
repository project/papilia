<?php

$content['type'] = array(
  'name' => 'Donation Campaign',
  'type' => 'papilia_campaign',
  'description' => 'Donation Campaign',
  'title_label' => 'Title',
  'body_label' => '',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array(
    'status' => TRUE,
    'promote' => FALSE,
    'sticky' => FALSE,
    'revision' => FALSE,
  ),
  'language_content_type' => '2',
  'old_type' => 'papilia_campaign',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'comment' => '0',
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
);
$content['fields']  = array(
  0 => 
  array(
    'label' => 'Papilia Form ID',
    'field_name' => 'field_papilia_form_id',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'change' => 'Change basic information',
    'weight' => '-4',
    'rows' => 5,
    'size' => '60',
    'description' => 'The FormId that you should have registered in Papilia Control Panel. Please make sure the formID is the same in all translations of this campaign.',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_papilia_form_id][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => FALSE,
    'required' => 1,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
  1 => 
  array(
    'label' => 'URI Path',
    'field_name' => 'field_puri',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'change' => 'Change basic information',
    'weight' => '-3',
    'rows' => 5,
    'size' => '60',
    'description' => 'Donation form will be available at the path indicated here. Please make sure the path is the same as in English, in all translated versions.',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_puri][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array(
      'field_puri' => 
      array(
        0 => 
        array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_puri][0][value',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 1,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
  2 => 
  array(
    'label' => 'Drupal Vocabulary Containing Allocation Funds List',
    'field_name' => 'field_fund_vocabulary',
    'type' => 'text',
    'widget_type' => 'optionwidgets_select',
    'change' => 'Change basic information',
    'weight' => '-2',
    'description' => 'You can override the default Allocation Fund Vocabulary (set in <a href="/admin/settings/papilia">Papilia Settings</a>)  here, if needed.',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array(
      'field_fund_vocabulary' => 
      array(
        'value' => '',
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => 'return papilia_fund_vocabularies();',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'optionwidgets',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
  3 => 
  array(
    'label' => 'Custom CSS',
    'field_name' => 'field_pcss',
    'type' => 'text',
    'widget_type' => 'text_textarea',
    'change' => 'Change basic information',
    'weight' => '-1',
    'rows' => '7',
    'size' => 60,
    'description' => 'Do not include &lt;style&gt; tags! Papilia form is wrapped with a "div" that has class "papilia_form". Please scope styles classes by making them children of "div.papilia_form" if you want to make sure they do not affect the rest of the page.',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_pcss][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array(
      'field_pcss' => 
      array(
        0 => 
        array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_pcss][0][value',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
  4 => 
  array(
    'label' => 'Header HTML',
    'field_name' => 'field_pheader',
    'type' => 'text',
    'widget_type' => 'text_textarea',
    'change' => 'Change basic information',
    'weight' => 0,
    'rows' => '20',
    'size' => 60,
    'description' => '',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_pheader][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array(
      'field_pheader' => 
      array(
        0 => 
        array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_pheader][0][value',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
  5 => 
  array(
    'label' => 'Footer HTML',
    'field_name' => 'field_pfooter',
    'type' => 'text',
    'widget_type' => 'text_textarea',
    'change' => 'Change basic information',
    'weight' => '2',
    'rows' => '5',
    'size' => 60,
    'description' => '',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_pfooter][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array(
      'field_pfooter' => 
      array(
        0 => 
        array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_pfooter][0][value',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
  6 => 
  array(
    'label' => 'Thank You Message',
    'field_name' => 'field_thankyou',
    'type' => 'text',
    'widget_type' => 'text_textarea',
    'change' => 'Change basic information',
    'weight' => '3',
    'rows' => '7',
    'size' => 60,
    'description' => '',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_thankyou][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array(
      'field_thankyou' => 
      array(
        0 => 
        array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_thankyou][0][value',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
  7 => 
  array(
    'label' => 'Image',
    'field_name' => 'field_pimages',
    'type' => 'image',
    'widget_type' => 'imagefield_widget',
    'change' => 'Change basic information',
    'weight' => '4',
    'file_extensions' => 'jpg jpeg png gif',
    'file_path' => '',
    'max_filesize_per_file' => '',
    'max_filesize_per_node' => '',
    'max_resolution' => 0,
    'min_resolution' => 0,
    'custom_alt' => 0,
    'alt' => '',
    'custom_title' => 0,
    'title' => '',
    'description' => '',
    'group' => FALSE,
    'required' => 0,
    'multiple' => '1',
    'list_default' => '0',
    'force_list_default' => '1',
    'show_description' => '1',
    'op' => 'Save field settings',
    'module' => 'imagefield',
    'widget_module' => 'imagefield',
    'columns' => 
    array(
      'fid' => 
      array(
        'type' => 'int',
        'not null' => FALSE,
      ),
      'list' => 
      array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
      ),
      'data' => 
      array(
        'type' => 'text',
        'serialize' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
);
