<?php

/**
 * @file
 *
 */

/**
* @test_mode
*   'true' or 'false'
*/
function papilia_make_call( $data, $email, $form_id = '', $test_mode = 'true' ) {
  global $language;
  if (isset($language->language)) {
    $data['LANGUAGES']=$language->language;
  }

  //$HOST    = "https://secure.preprod.papilia.com:8443"; //Preprod
  //$HOST = "https://secure.staging.papilia.com:8443"; //Staging
  $HOST = 'https://secure.my-websites.org'; //Prod
  $URI     = "/supporter/donationapi.do";
  $query    = array(
    'n' => get_papilia_mangled_id(),
    'dftm' => $test_mode,
    'e' => $email,
    'dfdbid' => $form_id,
  );
  $query_enc = http_build_query($query, '', '&');
  $URI .= "?" . $query_enc;
  
  $URL = $HOST . $URI;
    
  $headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded',
    'X-Forwarded-For' => ip_address(),    
  );
  $method  = 'POST';
  
  $data['API_SIGNATURE'] = get_papilia_api_signature();
  
  /**$data    = array(
    'API_SIGNATURE' => get_papilia_api_signature(),
    'AMT' => '1.00',
    'TYPE' => 'C/visa',
    'ACCT' => '4444',
    'EXPDATE' => '0220',
    'NAME' => 'Picktek LLC',
    'CVV2' => '033',
    'EMAIL' => 'idumali@gmail.com',
    'STREET' => '41 Vazha Pshavela ave',
    'ZIP' => '0177',
    'CITY' => 'Tbilisi',
    'COUNTRY' => 'GE',
    'CURRENCY' => 'USD',    
  );**/
  
  $data_enc = http_build_query($data, '', '&');
  $ret      = drupal_http_request($HOST . $URI, $headers, $method, $data_enc);
  
  return $ret;
  
//echo "<pre>".print_r ( $ret,true)."</pre>";
//exit();
}
