<?php

/**
 * @file
 *
 */
 
function papilia_delegate($url, $campaign) {

  global $_PAPILIA_ERRORS;
  global $language;

  $campaign_title = $campaign->title;

  //-- Multilinguality here
  $langcode = $language->language; //ISO2 code  
  $sql = 'SELECT n.nid FROM {node} n WHERE n.status = 1 and n.tnid = %d and n.language = \'%s\' ';
  $i18n_nid = db_result(db_query(db_rewrite_sql($sql), $campaign->nid, $langcode)); 
    
  if ( is_numeric($i18n_nid) && ($i18n_nid > 0)  ) {
    $node = node_load( $i18n_nid );
  }
  else {
    $node = node_load($campaign->nid);
  }

  drupal_set_title($node->title);
  
  $css = $node->field_pcss[0]['value'];
  $header = $node->field_pheader[0]['value'];
  $footer = $node->field_pfooter[0]['value'];    
  
  $mpath = drupal_get_path('module', 'papilia');
  require_once($mpath .'/papilia.countries.inc' );
  $form = drupal_get_form('papilia_donation_form', $campaign, $node);
  
  $out = '';
  $out = '<style>'. $css .'</style>';
  $out .= '<div class="papilia_form">';
  $out .= $header;
  
  $errors = $_PAPILIA_ERRORS;  
  unset( $_PAPILIA_ERRORS );
  
  $out .= theme_papilia_errors($errors);
  $out .= $form;
  $out .= $footer;  
  
  $out .= '</div>'; //end div[class]='papilia_form'
  return $out;
}

function theme_papilia_errors( $errors ) {
  if ( ! is_array($errors) || sizeof($errors) < 1 ) return "";
  
  $out = '<div class="message error"><ul>';
  foreach ( $errors['error'] as $err ) {
    $out .= '<li>'. $err .'</li>';
  }
  $out .= '</ul></div>';
  return $out;
}

function papilia_donation_form(&$form_state, $campaign, $campaign_node) {
  
  papilia_pre_submit_verification_resources($campaign->title);   
  
  $form['papilia_campaign_id'] = array(
    '#type' => 'hidden',
    '#value' => $campaign->nid,
  );
  
  $form['mainfields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Personal Information'),
    '#collapsible' => TRUE,    
  );
  
  $form['mainfields']['FIRSTNAME'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),    
    '#required' => TRUE,    
  );

  $form['mainfields']['MIDDLENAME'] = array(
    '#type' => 'textfield',
    '#title' => t('Middle Name'),        
  );

  $form['mainfields']['LASTNAME'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),        
    '#required' => TRUE,        
  );

  $form['mainfields']['EMAIL'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),        
    '#required' => TRUE,        
  );

  $form['money'] = array(
    '#type' => 'fieldset',
    '#title' => t('Donation Information'),
    '#collapsible' => TRUE,    
  );

   $form['money']['CURRENCY'] = array(
    '#type' => 'select',
    '#title' => t('Preferred Currency'),        
    '#options' =>   array('EUR' => t('Euros'),
                            "AUD" => t("Australian Dollars"),
                            "CAD" => t("Canadian Dollars"),
                            "GBP" => t("Pound Sterlings"),
                            "JPY" => t("Yens"),
                            "USD" => t("US Dollars"),
                            ),
    '#required' => TRUE,        
  );

  $form['money']['AMT'] = array(
    '#type' => 'textfield',
    '#title' => t('Donation Amount'), 
    '#description' => t('Format: 1000.00'),
    '#required' => TRUE,
  );
  
  $funds = papilia_get_funds($campaign_node);
  if ( $funds === FALSE ) { // not displaying
    $form['money']['DESCRIPTION'] = array(
      '#type' => 'hidden',
      '#default_value' => '',
    );  
  }
  else {
    $form['money']['DESCRIPTION'] = array(
      '#type' => 'select',
      '#title' => t('Choose fund allocation for your donation'),
      '#options' => papilia_get_funds($campaign_node),
    );
  }
  
  $form['billing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Billing Information'),
    '#collapsible' => TRUE,    
  );
  
  $cards_img_path =  drupal_get_path('module', 'papilia') .'/img/weacceptcards.png';
  $form['billing']['weaccept'] = array(
    '#value' => theme_image($cards_img_path),
  );
  
  $form['billing']['TYPE'] = array(
    '#type' => 'select',
    '#title' => t('Credit Card Type'),
    '#options' => array( 
        'C/visa' => t("Visa"),
        'C/mastercard' => t("Mastercard"),
        'C/american' => t("American Express"),
        'C/discover' => t("Discover"),
        'C/jcb' => t("JCB"),
        'C/dinersclub' =>  t("Diner's Club"),        
    ),
  );

  $form['billing']['ACCT'] = array(
    '#type' => 'textfield',
    '#title' => t('Card Number'),
    '#required' => TRUE,
  );

  $form['billing']['NAME'] = array(
    '#type' => 'textfield',
    '#title' => t('Name on the card'),
    '#required' => TRUE,
  );

  $form['billing']['EXPDATE'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiration Date'),
    '#required' => TRUE,
    '#size' => 4, '#maxlength' => 4,
    '#description' => t('Four digits in MMYY format: two-digit month and two-digit year.'),
  );

  $form['billing']['CVV2'] = array(
    '#type' => 'textfield',
    '#title' => t('CVV2'),
    '#size' => 3, '#maxlength' => 7,
    '#required' => TRUE,
    '#description' => t('Security code usually located on the back of your card'),
  );
  
  $form['billingaddr'] = array(
    '#type' => 'fieldset',
    '#title' => t('Billing Address'),
    '#collapsible' => TRUE,
  );

  $form['billingaddr']['STREET'] = array(
    '#type' => 'textfield',
    '#title' => t('Street'),
    '#required' => TRUE,
  );

  $form['billingaddr']['CITY'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#required' => TRUE,
  );

  $states = array('none' => t('Outside the US'));
  $states = $states  + papilia_get_us_states();

  $form['billingaddr']['STATE'] = array(
    '#type' => 'select',
    '#title' => t('State'),
    '#options' => $states,
    '#required' => TRUE,
  );

  $form['billingaddr']['ZIP'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip/Postal Code'),
    '#size' => 5, '#maxlength' => 10,
    '#required' => TRUE,
  );


  $form['billingaddr']['COUNTRY'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => papilia_get_countries(),
    '#required' => TRUE,
  );

  $form['billingaddr']['PHONENUM'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
  );

  $form['papilia_dialog'] = array(
    '#value' => '<div id="papilia_dialog" class="papiliadialog"></div>',
  );
  
/**  $form['transact'] = array(
    '#value' => '<button id="donation_button">' . t('Send Payment') . '</button>',
  );**/
  
  $form['submit'] = array(
    '#prefix' => '<div class="papilia_submission">',
    '#type' => 'submit',
    '#value' => t('Send Payment'),
    '#suffix' => '</div>',
  );
  
  return $form;
}

function papilia_donation_form_validate($form, &$form_state) {
  global $_PAPILIA_ERRORS;
  
  $post = $form_state['values'];
  
  $zip_code_problem = FALSE;
  if ($post['COUNTRY'] == 'US' && $post['STATE'] == 'none') {
   $zip_code_problem = TRUE;
  }
  
  if ( ! $zip_code_problem ) {
    $post = $form_state['values'];
    
    $test_mode = papilia_is_test_mode();
    $ret = papilia_call_wrapper($post, $test_mode); 
    $status = $ret->status;
  } 
  else { $status = TRUE; } //sudo status=TRUE since we don't even check it when zip code problem exists

  if ( $zip_code_problem ) {
    form_set_error('STATE', t('You have to indicate a state for US.'));
  }
  
  if ( ! $status ) {
    form_set_error('form_papilia_id', $ret->msg);
  }
  
  $errors = drupal_get_messages(null, TRUE);
  $_PAPILIA_ERRORS = $errors;  
    
  
}

function papilia_donation_form_submit($form, &$form_state) {

  unset($form_state['rebuild']);
  $form_state['redirect'] = 'papilia/thankyou';
  
  $msg =  t('Your donation has been successfully received. Thank you!');
  drupal_set_message( $msg, 'status' );

}

function papilia_generate_form_id($nid) {

  $res = db_fetch_object(db_query('SELECT nid, language, tnid from {node} WHERE status = 1 and nid = %d', $nid));
  
  if ( empty($res->language) ) { 
    $node_id = $res->nid; 
  } 
  else {
    $node_id = $res->tnid;
  }
  
  $node = node_load($node_id);
    
  $fid = $node->field_papilia_form_id[0]['value'];
  return $fid;
}

/**
* Operations needed by both validate() and submit(), but validate() always launches in test mode.
*/
function papilia_call_wrapper( $post, $test_mode ) {
  
  $response = new stdClass();
 
  unset($post['op']); 
  unset($post['submit']);
  unset($post['form_build_id']); 
  unset($post['form_token']); 
  unset($post['form_id']);
  
  $email = $post['EMAIL'];
  
  $campaign_id = $post['papilia_campaign_id'];
  unset($post['form_papilia_id']);
  
  if ( $post['STATE'] == 'none' ) {
    unset($post['STATE']);
  }
  
  if ( strpos($post['AMT'], '.' ) === FALSE ) { 
    $post['AMT'] .= ".00";
  }
    
    $form_id = papilia_generate_form_id($post['papilia_campaign_id']);

  $mpath = drupal_get_path('module', 'papilia');
  require_once( $mpath .'/papilia.client.inc');
  $ret = papilia_make_call( $post, $email, $form_id, $test_mode ); 
  
  switch ( $ret->code ) {
    
    case 201: //success
    case 202: //success    
      $call_resp = simplexml_load_string( $ret->data );  
      $prop = "papilia-gift"; //The dash in the property name creates problems unless in a separate var like this
      $refnum = (string) $call_resp->$prop->reference; //->reference;
      $_SESSION["papilia_trx_refnum"] = $refnum; 
      $_SESSION["papilia_campaign_id"] = $campaign_id;       
      $response->status = 1;
      
      // Log successful transaction for security purposes (no CC info saved).
      $tx_node = new stdClass();
      $name = $post['FIRSTNAME'] .' '. $post['MIDDLENAME'] .' '. $post['LASTNAME'];
      $tx_node->title = $name ." TX Date: ". date("F j, Y, g:i a", time());
      $tx_node->field_pt_name[0]['value'] =  $name;
      $tx_node->field_pt_txid[0]['value'] =  $refnum;      
      $tx_node->field_pt_email[0]['value'] =  $post['EMAIL'];
      $tx_node->field_pt_amount[0]['value'] =  $post['AMT'] .' '. $post['CURRENCY'];      
      $tx_node->type = 'papilia_transaction';
      $tx_node->language = 'en';
      $tx_node->uid = 1;
      $tx_node->status = 0;      
      node_save($tx_node);
      
      return $response;
          
    case 200: //payment denied
      if ( ! $ret->error || strlen(trim($ret->error)) == 0 ) {
        $errmsg1 = $ret->data;
      } 
      else {
        $errmsg1 = "\n<p>". $ret->error;
      }
      $msg =  t('Payment did not go through. Your card was declined. Please double-check the information and try again.') . $errmsg1;
      break;
    
    default:
      $msg =  t('An error occured while trying to process your request: ') . $ret->error; 
      $watchdog_variables = array(
         '%code'  => $ret->code,
         '%error' => $ret->error,
         '%data'  => $ret->data,
      );      
      watchdog('papilia', "Error (%code): %error<br /><pre>%data</pre>", $watchdog_variables, WATCHDOG_ERROR);
      break;
  }
  
  //succes returns immediately above,so if you get here, it's an error.
  $response->status = 0;
  $response->msg = $msg;
  return $response;

}

function papilia_campaigns_list() {
  
  $campaigns = papilia_get_campaigns();
 
  if ( !is_array($campaigns) || sizeof($campaigns) < 1 ) {
    return t("No campaigns in the system, yet.");
  }
  
  $out = "
<table>
  <tr>
    <th>". t("Title") ."</th>
    <th>". t("Enabled") ."</th>     
    <th colspan=\"2\" align=\"center\">". t("Control") ."</th>                      
  </tr>
  ";
  
  $options = array('query' => 'destination=admin/settings/papilia/list');
  
  foreach ( $campaigns as $obj ) {
    $out .="<tr>";
    $out .= "<td>". l($obj->title, get_papilia_url_prefix() ."/". $obj->url) ."</td>";
    $enabled = ( $obj->status ) ? t("Yes") : t("No");
    $out .= "<td>". $enabled ."</td>"; 
    $out .= "<td>". l(t('Edit'), 'node/'. $obj->nid .'/edit', $options) ."</td>"; 
    $out .= "<td>". l(t('Delete'), 'node/'. $obj->nid .'/delete', $options) ."</td>";         
    $out .= "</tr>";    
  }
  
  $out .= "</table>";
  
  return $out;
}

function papilia_load( $nid ) {
  return node_load($nid);
}

function papilia_thankyou( $confirmation='invalid request' ) {

  $refnum = $_SESSION["papilia_trx_refnum"];
  unset($_SESSION["papilia_trx_refnum"]);
  
  $campaign_id = $_SESSION["papilia_campaign_id"];
  unset($_SESSION["papilia_campaign_id"]);  

    
  if (empty($refnum) || empty($campaign_id)) {
    $msg = t("Invalid Request!");
    drupal_set_title( $msg );
    return t('You are not supposed to access this page directly');
  }

  // Default Message:
  $msg = t("Confirmation Number:") ." ". $refnum ."<br/>" .
         t("Please write it down for your records") ;
         
  // Customized:
    
  $campaign_node = node_load( $campaign_id);
  if (is_object($campaign_node)) {
    $thankyou = $campaign_node->field_thankyou[0]['value'];
    $thankyou = str_replace('%confirmation', $refnum, $thankyou);
    $thankyou = str_replace('%campaign', $campaign_node->title, $thankyou);
    $msg = check_markup($thankyou, FILTER_HTML_ESCAPE);
  }

         
  return $msg;         
}

