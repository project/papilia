/** Drupal.behaviors.papiliaBehavior = function (context) {
  //alert(context)
     
   $("#donation_button").click(function(e) {
      e.preventDefault();   
      alert( "opa" );
       papilia_modaltext_display()
        
       papilia_showmodal()
       //alert("Hello world!");
       if ( ! $("#papilia_dialog").dialog( "isOpen" ) ) {
         $("#papilia_dialog").dialog("open");
       }
   });
};**/

jQuery(document).ready(function() { 

   $(".papilia_submission input").click(function(e) {
       e.preventDefault();
              
       papilia_modaltext_display()
        
       papilia_showmodal()
       //alert("Hello world!");
       if ( ! $("#papilia_dialog").dialog( "isOpen" ) ) {
         $("#papilia_dialog").dialog("open");
       }

   });


});

function papilia_showmodal() {

  buttons = {};
  buttons[pp_proceed] = function() { 
    //alert("Ok"); 
    $('.ui-dialog-buttonpane').hide();        
    $('#papilia-donation-form').get(0).submit();
  }

  buttons[pp_cancel] = function() { 
    $(this).dialog("close"); 
  }


  $("#papilia_dialog").dialog({ 
      modal: true,
      overlay: { 
        opacity: 0.8, 
        background: "black" 
      },
      buttons: buttons
  });

}

function papilia_modaltext_display() {

  var amt = ( $("input#edit-AMT").val() > 0 ) ? $("input#edit-AMT").val() : '0';
  var currency =  $('#edit-CURRENCY > option[@selected]').text();
  
   var pp_msg = "<span class=\"ppmsg\">";
   pp_msg += pp_txt1 + " <div class=\"ppamount\">" + amt + " " + currency +  "</div> " + pp_txt2;
   pp_msg += "</span>";  
   
   $("div#papilia_dialog").html(pp_msg);
}