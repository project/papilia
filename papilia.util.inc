<?php

/**
 * @file
 *
 */
 
/**
* A wrapper around papilia_get_vocabularies() adding the "use global" option.
*/
function papilia_fund_vocabularies() {
  $funds = papilia_get_vocabularies();
  $mfunds= array('global' => '- '. t('Use Global Setting') .' - ' ) ;
  if ( is_array($funds) && sizeof($funds) > 0 ) {
    foreach ($funds as $key => $val ) {
      $mfunds[$key] = $val;
    }
  }
  else { //no vocabularies exist, global setting is bunk as well
    return array();
  }
  
  return $mfunds;
}

function papilia_get_funds(&$campaign_node) {

  $vid = $campaign_node->field_fund_vocabulary[0]['value'];
    
  if (empty($vid)) return FALSE; //do not display fund allocation
  
  if ($vid == 'global') { //use default setting
    $vid = get_papilia_donation_fund();
  }

  $terms = taxonomy_get_tree($vid);

  $terms_options = array('---' => t('No specific fund allocation')); //reformat
  if ( !is_array($terms) || sizeof($terms)<1 ) return $terms_options;
  foreach ($terms as $t) {
    $terms_options[$t->name] = t($t->name);
  }
  return $terms_options;
}

function papilia_get_vocabularies() {
  $sql = "SELECT vid, name FROM {vocabulary}";
  $res = db_query($sql);
  $vocabs = array();
  while ( $obj = db_fetch_object($res) ) {
    $vocabs[$obj->vid] = $obj->name;
  }
  return $vocabs;
}

function get_papilia_mangled_id() {
  return variable_get('papilia_mangled_id', 'FXss');
}

function get_papilia_api_signature() {
  return variable_get('papilia_api_signature', 'XXXXXXXXXXXXXXX');
}

function get_papilia_donation_fund() {
  return variable_get('papilia_donation_fund', '');
}

function papilia_is_test_mode() {
  $ret = variable_get('papilia_test_mode', '1') ? 'true' : 'false';
  return $ret;
}
