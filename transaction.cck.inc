<?php

$content['type']  = array(
  'name' => 'Papilia Transaction',
  'type' => 'papilia_transaction',
  'description' => 'Logged record of a Papilia Transaction',
  'title_label' => 'Title',
  'body_label' => '',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array(
    'status' => FALSE,
    'promote' => FALSE,
    'sticky' => FALSE,
    'revision' => FALSE,
  ),
  'language_content_type' => '0',
  'old_type' => 'papilia_transaction',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'comment' => '0',
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
);
$content['fields']  = array(
  0 => 
  array(
    'label' => 'Donor Name',
    'field_name' => 'field_pt_name',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'change' => 'Change basic information',
    'weight' => '-4',
    'rows' => 5,
    'size' => '60',
    'description' => '',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_pt_name][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => FALSE,
    'required' => 1,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
  1 => 
  array(
    'label' => 'Transaction/Serial #',
    'field_name' => 'field_pt_txid',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'change' => 'Change basic information',
    'weight' => '-3',
    'rows' => 5,
    'size' => '60',
    'description' => '',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_pt_txid][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array(
      'field_pt_txid' => 
      array(
        0 => 
        array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_pt_txid][0][value',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 1,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
  2 => 
  array(
    'label' => 'Donation Amount',
    'field_name' => 'field_pt_amount',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'change' => 'Change basic information',
    'weight' => '-2',
    'rows' => 5,
    'size' => '60',
    'description' => '',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_pt_amount][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array(
      'field_pt_amount' => 
      array(
        0 => 
        array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_pt_amount][0][value',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 1,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
  3 => 
  array(
    'label' => 'Donor\'s E-Mail',
    'field_name' => 'field_pt_email',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'change' => 'Change basic information',
    'weight' => '-1',
    'rows' => 5,
    'size' => '60',
    'description' => '',
    'default_value' => 
    array(
      0 => 
      array(
        'value' => '',
        '_error_element' => 'default_value_widget][field_pt_email][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array(
      'field_pt_email' => 
      array(
        0 => 
        array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_pt_email][0][value',
        ),
      ),
    ),
    'group' => FALSE,
    'required' => 1,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array(
      'value' => 
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
      ),
    ),
    'display_settings' => 
    array(
      'label' => 
      array(
        'format' => 'above',
      ),
      'teaser' => 
      array(
        'format' => 'default',
      ),
      'full' => 
      array(
        'format' => 'default',
      ),
      4 => 
      array(
        'format' => 'default',
      ),
    ),
  ),
);
